package net.amtiskaw.lox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import static net.amtiskaw.lox.TokenType.*;

public class Parser {
    private static class ParseError extends RuntimeException {}

    private final List<Token> tokens;
    private int current = 0;

    /*
    Our grammar:
    ------------
    programme      → declaration* EOF ;
    declaration    → classDecl
                   | funDecl
                   | varDecl
                   | statement ;
    classDecl      → class IDENTIFIER "{" function* "}" ;
    funDecl        → "fun" function;
    function       → IDENTIFIER "(" parameters? ")" block ;
    varDecl        → "var" IDENTIFIER ( "=" expression )? ";" ;
    statement      → exprStmt
                   | forStmt
                   | ifStmt
                   | printStmt
                   | returnStmt
                   | whileStmt
                   | block ;
    forStmt        → "for" "(" ( varDecl | expression | ";" ) expression? ";" expression? ")" statement ;
    ifStmt         → "if" "(" expression ")" statement ( "else" statement )? ;
    whileStmt      → "while" "(" expression ")" statement ;
    block          → "{" declaration* "}" ;
    exprStmt       → expression ";" ;
    printStmt      → "print" expression? ";" ;
    returnStmt     → "return" expression ";" ;
    expression     → comma ;
    comma          → ternary ( "," ternary )* ;
    ternary        → assignment ( "?" expression ":" assignment)? ;
    assignment     → ( call "." )? IDENTIFIER "=" assignment
                   | logic_or ;
    logic_or       → logic_and ( "or" logic_and )* ;  // and has higher precedence.
    logic_and      → equality ( "and" equality )* ;
    equality       → comparison ( ( "!=" | "==" ) comparison )* ;
    comparison     → addition ( ( ">" | ">=" | "<" | "<=" ) addition )* ;
    addition       → multiplication ( ( "-" | "+" ) multiplication )* ;
    multiplication → unary ( ( "/" | "*" ) unary )* ;
    unary          → ( "!" | "-" ) unary
                   | call ;
    call           → lambda ( "(" arguments? ")" | "." IDENTIFIER )* ;
    arguments      → expression ( "," expression )* ;
    lambda         → "fn" "(" arguments? ")" block
                   | primary ;
    primary        → "true" | "false" | "nil"
                   | NUMBER | STRING
                   | "(" expression ")"
                   | IDENTIFIER ;
    */

    Parser(List<Token> tokens) {
        this.tokens = tokens;
    }

    List<Stmt> parse() {
        List<Stmt> statements = new ArrayList<>();
        while (!isAtEnd()) {
            statements.add(declaration());
        }
        return statements;
    }

    private Stmt statement() {
        if (match(FOR)) return forStatement();
        if (match(IF)) return ifStatement();
        if (match(WHILE)) return whileStatement();
        if (match(PRINT)) return printStatement();
        if (match(RETURN)) return returnStatement();
        if (match(LEFT_BRACE)) return new Stmt.Block(block()); // block() returns a List<Stmt>
        else return expressionStatement();
    }

    private Stmt forStatement() {
        consume(LEFT_PAREN, "Expect '(' after 'for'.");

        // Optional INITIALIZER
        Stmt initializer;
        if (match(SEMICOLON)) {
            initializer = null;
        } else if (match(VAR)) {
            initializer = varDeclaration();
        } else {
            initializer = expressionStatement();
        }

        // Optional CONDITION
        Expr condition = null;
        if (!check(SEMICOLON)) {
            condition = expression();
        }
        consume(SEMICOLON, "Expect ';' after 'for' condition.");

        // Optional INCREMENT
        Expr increment = null;
        if (!check(RIGHT_PAREN)) {
            increment = expression();
        }
        consume(RIGHT_PAREN, "Expect ')' before 'for' body.");

        // BODY
        Stmt body = statement();

        /*
         Build a while loop in a block:

             {
                initializer;
                while (condition) {
                    body;
                    increment;
                }
             }
         */

        if (increment != null) {
            body = new Stmt.Block(Arrays.asList(
                    body,
                    new Stmt.Expression(increment)));
        }

        // If the condition is missing, sub in 'true'.
        if (condition == null) {
            condition = new Expr.Literal(true);
        }

        // Return a new outer block containing the initializer and while loop.
        List<Stmt> forBlock = new ArrayList<>();
        if (initializer != null) {
            forBlock.add(initializer);
        }
        forBlock.add(new Stmt.While(condition, body));
        return new Stmt.Block(forBlock);
    }

    private Stmt ifStatement() {
        consume(LEFT_PAREN, "Expect '(' after 'if'.");
        Expr condition = expression();
        consume(RIGHT_PAREN, "Expect ')' after 'if' condition.");

        Stmt thenClause = statement();
        Stmt elseClause = null;

        if (match(ELSE)) {
            elseClause = statement();
        }

        return new Stmt.If(condition, thenClause, elseClause);
    }

    private Stmt whileStatement() {
        consume(LEFT_PAREN, "Expect '(' after 'while'.");
        Expr condition = expression();
        consume(RIGHT_PAREN, "Expect ')' after 'while' condition.");

        Stmt body = statement();

        return new Stmt.While(condition, body);
    }

    private List<Stmt> block() {
        List<Stmt> statements = new ArrayList<>();

        while (!check(RIGHT_BRACE) && !isAtEnd()) {
            statements.add(declaration());
        }

        consume(RIGHT_BRACE, "Expect closing '}' for block.");
        return statements;
    }

    private Stmt printStatement() {
        Expr value = expression();
        consume(SEMICOLON, "Expect ';' after value.");
        return new Stmt.Print(value);
    }

    private Stmt returnStatement() {
        Token keyword = previous(); // capture the "return" token.

        Expr result = null;
        if (!check(SEMICOLON)) {
            // Expect an expression to return.
            result = expression();
        }
        consume(SEMICOLON, "Expect ';' after return.");

        return new Stmt.Return(keyword, result);
    }

    private Stmt varDeclaration() {
        Token name = consume(IDENTIFIER, "Expected a variable name.");

        Expr initializer = null; // uninitialised variable.
        if (match(EQUAL)) {
            initializer = expression();
        }

        consume(SEMICOLON, "Expected ';' after a variable declaration.");
        return new Stmt.Var(name, initializer);
    }

    private Stmt expressionStatement() {
        Expr expr = expression();
        consume(SEMICOLON, "Expect ';' after expression.");
        return new Stmt.Expression(expr);
    }

    private Stmt.Function function(String kind) {
        Token name = consume(IDENTIFIER, "Expected a " + kind + " name.");
        Expr.Function expr = finishFunction(kind);
        return new Stmt.Function(name, expr);
    }

    private Expr.Function finishFunction(String kind) {
        consume(LEFT_PAREN, "Expected parenthesized parameter list.");

        // Parse (optional) parameter list.
        List<Token> parameters = new ArrayList<>();
        if (!check(RIGHT_PAREN)) {
            boolean once = true;
            do {
                if (once && parameters.size() >= 255) {
                    error(peek(), "Cannot have more than 255 parameters.");
                    once = false;
                }
                parameters.add(consume(IDENTIFIER, "Expected parameter name"));
            } while (match(COMMA));
        }

        // The closing parenthesis
        consume(RIGHT_PAREN, "Expected closing parenthesis for " + kind + " arguments.");

        // NB. block() assumes the opening '{' has already been consumed.
        consume(LEFT_BRACE, "Expected " + kind + " body block.");
        List<Stmt> body = block();
        return new Expr.Function(parameters, body);
    }

    private Stmt declaration() {
        try {
            if (match(CLASS)) return classDeclaration();
            if (match(FUN)) return function("function");
            if (match(VAR)) return varDeclaration();
            return statement();
        } catch (ParseError error) {
            synchronize(); // don't give up completely, try to resync.
            return null;
        }
    }

    private Stmt classDeclaration() {
        Token name = consume(IDENTIFIER, "Expect class name");
        consume(LEFT_BRACE, "Expect '{' before class body");

        // List of zero or more methods.
        List<Stmt.Function> methods = new ArrayList<>();
        while (!check(RIGHT_BRACE) && !isAtEnd()) {
            methods.add(function("method"));
        }

        consume(RIGHT_BRACE, "Expect '}' after class body");

        return new Stmt.Class(name, methods);
    }

    private Expr expression() {
        return ternary();
    }

    private Expr binary(Supplier<Expr> operandType, TokenType... tokens) {
        Expr expr = operandType.get();

        // Match a sequence of zero or more "token operandType".
        while (match(tokens)) {
            Token operator = previous();
            Expr right = operandType.get();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    // ternary → equality ( "?" expression ":" equality)? ;
    private Expr ternary() {
        Expr expr = assignment();

        if (match(QUESTION_MARK)) {
            Expr trueClause = expression();
            consume(COLON, "expected colon");
            Expr falseClause = assignment();
            return new Expr.Ternary(expr, trueClause, falseClause);
        }

        return expr;
    }

    private Expr assignment() {
        Expr expr = or(); // r-value

        if (match(EQUAL)) {
            Token equals = previous();
            Expr value = assignment();

            if (expr instanceof Expr.Variable) {
                // Convert expr to an l-value Variable.
                Token name = ((Expr.Variable)expr).name;
                return new Expr.Assign(name, value);
            } else if (expr instanceof  Expr.Get) {
                // Convert the "Get" expression l-value into a "Set" expression.
                Expr.Get get = (Expr.Get)expr;
                return new Expr.Set(get.object, get.name, value);
            }

            // Don't throw, allowing parsing to continue at the next token.
            error(equals, "Invalid assignment target.");
        }

        return expr; // not an assignment.
    }

    private Expr or() {
        Expr expr = and();

        while (match(OR)) {
            Token operator = previous();
            Expr right = and();
            expr = new Expr.Logical(expr, operator, right);
        }

        return expr;
    }

    private Expr and() {
        Expr expr = equality();

        while (match(AND)) {
            Token operator = previous();
            Expr right = equality();
            expr = new Expr.Logical(expr, operator, right);
        }

        return expr;
    }

    private Expr comma() {
        // FIXME: comma operator conflicts with function calls - disabled for now.
        return binary(() -> ternary(), COMMA);
    }

    private Expr equality() {
        return binary(() -> comparison(), BANG_EQUAL, EQUAL_EQUAL);
    }

    private Expr comparison() {
        return binary(() -> addition(), GREATER, GREATER_EQUAL, LESS, LESS_EQUAL);
    }

    private Expr addition() {
        return binary(() -> multiplication(), MINUS, PLUS);
    }

    private Expr multiplication() {
        return binary(() -> unary(), SLASH, STAR);
    }

    private Expr unary() {
        if (match(BANG, MINUS)) {
            Token operator = previous();
            Expr right = unary();
            return new Expr.Unary(operator, right);
        }

        return call();
    }

    private Expr call() {
        Expr expr = lambda();

        // Match one or more calls.
        while (true) {
            if (match(LEFT_PAREN)) {
                expr = finishCall(expr); // Where expr becomes the callee
            } else if (match(DOT)) { // name.xyz ...
                Token name = consume(IDENTIFIER,
                        "Expect property name after '.'");
                expr = new Expr.Get(expr, name);
            } else {
                break;
            }
        }

        return expr;
    }

    private Expr lambda() {
        if (match(FUN)) {
            return finishFunction("lambda");
        }
        return primary();
    }

    private Expr finishCall(Expr callee) {
        List<Expr> arguments = new ArrayList<>();
        if (!check(RIGHT_PAREN)) {
            boolean once = true;
            do {
                arguments.add(expression());
                if (arguments.size() >= 255 && once) { // same limit as Java
                    error(peek(), "Cannot have more than 255 arguments.");
                    once = false;
                }
            } while (match(COMMA));
        }

        Token rparen = consume(RIGHT_PAREN, "Expected closing ')' after arguments.");
        return new Expr.Call(callee, rparen, arguments);
    }

    private Expr primary() {
        if (match(FALSE)) return new Expr.Literal(false);
        if (match(TRUE))  return new Expr.Literal(true);
        if (match(NIL))   return new Expr.Literal(null);

        if (match(NUMBER, STRING)) {
            return new Expr.Literal(previous().literal); // matched literal
        }

        if (match(THIS)) {
            return new Expr.This(previous());
        }

        if (match(IDENTIFIER)) {
            return new Expr.Variable(previous()); // construct using the IDENTIFIER just consumed.
        }

        if (match(LEFT_PAREN)) {
            Expr expr = expression();
            consume(RIGHT_PAREN, "Expect ')' after expression.");
            return new Expr.Grouping(expr);
        }

        throw error(peek(), "expected expression");
    }

    private boolean match(TokenType... types) {
        for (TokenType type : types) {
            if (check(type)) {
                advance();
                return true;
            }
        }
        return false;
    }

    private Token consume(TokenType type, String message) {
        if (check(type)) return advance();
        throw error(peek(), message);
    }

    private boolean check(TokenType type) {
        if (isAtEnd()) return false;
        return peek().type == type;
    }

    private Token advance() {
        if (!isAtEnd()) current++;
        return previous();
    }

    private boolean isAtEnd() {
        return peek().type == EOF;
    }

    private Token peek() {
        return tokens.get(current);
    }

    private Token previous() {
        return tokens.get(current - 1);
    }

    private ParseError error(Token token, String message) {
        Lox.error(token, message);
        return new ParseError();
    }

    // Consume tokens after error until we reach a
    // synchronizing statement where parsing may resume.
    private void synchronize() {
        advance();

        while (!isAtEnd()) {
            // End of statement is a sync point.
            if (previous().type == SEMICOLON) return;

            switch (peek().type) {
                case CLASS:
                case FUN:
                case VAR:
                case FOR:
                case IF:
                case WHILE:
                case PRINT:
                case RETURN:
                    return;
            }

            advance();
        }
    }
}
