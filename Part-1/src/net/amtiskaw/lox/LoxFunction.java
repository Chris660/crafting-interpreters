package net.amtiskaw.lox;

import java.util.List;

public class LoxFunction implements LoxCallable {
    private final String identifier;
    private final Expr.Function declaration;
    private final Environment closure;

    LoxFunction(String identifier, Expr.Function declaration, Environment enclosing) {
        this.identifier = identifier;
        this.declaration = declaration;
        this.closure = enclosing;
    }

    public LoxFunction bind(LoxInstance instance) {
        // Bind the `instance` to the "this" variable in a new environment (closure).
        Environment environment = new Environment(closure);
        environment.define("this", instance);
        return new LoxFunction(identifier, declaration, environment);
    }

    @Override
    public int arity() {
        return declaration.params.size();
    }

    @Override
    public Object call(Interpreter interpreter, List<Object> arguments) {
        Environment environment = new Environment(closure);

        for (int i = 0 ; i < declaration.params.size() ; ++i) {
            environment.define(declaration.params.get(i).lexeme, arguments.get(i));
        }

        try {
            interpreter.executeBlock(declaration.body, environment);
        } catch (Return result) {
            return result.value;
        }
        return null;
    }

    @Override
    public String toString() {
        return "<fn " + identifier + ">";
    }
}
