package net.amtiskaw.lox;

// Encapsulates a value returned by a LoxCallable.
public class Return extends RuntimeException {
    final Object value;

    public Return(Object result) {
        // This is not an error, so disable stack-tracing etc.
        super(null, null, false, false);
        value = result;
    }
}
