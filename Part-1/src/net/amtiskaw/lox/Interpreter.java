package net.amtiskaw.lox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Interpreter implements Expr.Visitor<Object>, Stmt.Visitor<Void> {
    final Environment globals = new Environment();
    private Environment environment = globals;

    /*
     * The locals map records the *depth* of scope for each variable reference.
     */
    private final Map<Expr, Integer> locals = new HashMap<>();

    Interpreter() {
        globals.define("clock", new LoxCallable() { // anonymous class implementing LoxCallable
            @Override
            public int arity() {
                return 0;
            }

            @Override
            public Object call(Interpreter interpreter, List<Object> arguments) {
                return (double)System.nanoTime() / 1e9;
            }

            @Override
            public String toString() {
                return "<native fn>";
            }
        });
    }

    public void interpret(List<Stmt> statements) {
        try {
            for (Stmt stmt : statements) {
                execute(stmt);
            }
        } catch (RuntimeError error) {
            Lox.runtimeError(error);
        }
    }

    private void execute(Stmt stmt) {
        stmt.accept(this); // Visit the statement.
    }

    public void executeBlock(List<Stmt> statements, Environment env) {
        Environment previous = this.environment; // Save the current environment.
        try {
            // XXX: swapping out `environment` is not so nice.
            this.environment = env;
            for (Stmt stmt : statements) {
                execute(stmt);
            }
        } finally {
            // Whatever happens restore the previous environment.
            this.environment = previous;
        }
    }

    public void resolve(Expr expr, int depth) {
        locals.put(expr, depth);
    }

    @Override
    public Void visitPrintStmt(Stmt.Print stmt) {
        Object value = evaluate(stmt.expression);
        System.out.println(stringify(value));
        return null;
    }

    @Override
    public Void visitReturnStmt(Stmt.Return stmt) {
        // Abuse Java exceptions to return the from the current LoxCallable.
        Object result = null;
        if (stmt.expression != null) result = evaluate(stmt.expression);
        throw new Return(result);
    }

    @Override
    public Void visitVarStmt(Stmt.Var stmt) {
        Object value = null;
        if (stmt.initializer != null) {
            value = evaluate(stmt.initializer);
        }
        environment.define(stmt.name.lexeme, value);
        return null;
    }

    @Override
    public Void visitWhileStmt(Stmt.While stmt) {
        while (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.body);
        }
        return null;
    }

    @Override
    public Void visitBlockStmt(Stmt.Block stmt) {
        executeBlock(stmt.statements, new Environment(environment));
        return null;
    }

    @Override
    public Void visitClassStmt(Stmt.Class stmt) {
        environment.define(stmt.name.lexeme, null);

        Map<String, LoxFunction> methods = new HashMap<>();
        for (Stmt.Function method : stmt.methods) {
            LoxFunction function = new LoxFunction(method.name.lexeme, method.function, environment);
            methods.put(method.name.lexeme, function);
        }

        LoxClass klass = new LoxClass(stmt.name.lexeme, methods);
        environment.assign(stmt.name, klass);
        return null;
    }

    @Override
    public Void visitExpressionStmt(Stmt.Expression stmt) {
        evaluate(stmt.expression);
        return null;
    }

    @Override
    public Void visitFunctionStmt(Stmt.Function stmt) {
        // Remember this is a function *definition*, bind a new named
        // LoxFunction instance in the current environment.
        LoxFunction function = new LoxFunction(stmt.name.lexeme, stmt.function, environment);
        environment.define(stmt.name.lexeme, function);
        return null;
    }

    @Override
    public Void visitIfStmt(Stmt.If stmt) {
        if (isTruthy(evaluate(stmt.condition))) {
            execute(stmt.thenBranch);
        } else if (stmt.elseBranch != null) { // else is optional
            execute(stmt.elseBranch);
        }
        return null;
    }

    @Override
    public Object visitLiteralExpr(Expr.Literal expr) {
        // The value has already been evaluated by the Parser.
        return expr.value;
    }

    @Override
    public Object visitLogicalExpr(Expr.Logical expr) {
        Object left = evaluate(expr.left);

        switch (expr.operator.type) {
            case OR:
                if (isTruthy(left)) return left;
                break;

            case AND:
                if (!isTruthy(left)) return left;
                break;

            default:
                throw new RuntimeError(expr.operator, "Unknown logical operator!");
        }

        return evaluate(expr.right);
    }

    @Override
    public Object visitSetExpr(Expr.Set expr) {
        Object object = evaluate(expr.object);

        if (!(object instanceof LoxInstance)) {
            throw new RuntimeError(expr.name, "Only instances have fields.");
        }

        Object value = evaluate(expr.value);
        ((LoxInstance)object).set(expr.name, value);
        return value;
    }

    @Override
    public Object visitGroupingExpr(Expr.Grouping expr) {
        return evaluate(expr.expression);
    }

    @Override
    public Object visitUnaryExpr(Expr.Unary expr) {
        // Evaluate the rhs expression first.
        Object rhs = evaluate(expr.right);

        switch (expr.operator.type) {
            case BANG:
                return !isTruthy(rhs);

            case MINUS:
                assertNumericOperand(expr.operator, rhs);
                return -(double)rhs;

            default:
                return null;
        }
    }

    @Override
    public Object visitVariableExpr(Expr.Variable expr) {
        return lookupVariable(expr.name, expr);
    }

    private Object lookupVariable(Token name, Expr expr) {
        Integer distance = locals.get(expr);
        if (distance != null) {
            // This *is* a local variable.
            return environment.getAt(distance, name.lexeme);
        } else {
            return globals.get(name);
        }
    }

    @Override
    public Object visitAssignExpr(Expr.Assign expr) {
        Object value = evaluate(expr.value);

        // Find the appropriately scoped target for the assignment.
        Integer distance = locals.get(expr);
        if (distance != null) { // a local variable
            environment.assignAt(distance, expr.name, value);
        } else {
            globals.assign(expr.name, value);
        }

        return value; // as in C
    }

    @Override
    public Object visitBinaryExpr(Expr.Binary expr) {
        // Evaluate LHS and RHS operands, then apply the operator.
        Object lhs = evaluate(expr.left);
        Object rhs = evaluate(expr.right);

        switch (expr.operator.type) {
            case MINUS:
                assertNumericOperands(expr.operator, lhs, rhs);
                return (double)lhs - (double)rhs;
            case SLASH:
                assertNumericOperands(expr.operator, lhs, rhs);
                if ((double)rhs == 0.0) {
                    throw new RuntimeError(expr.operator, "divide by zero!");
                }
                return (double)lhs / (double)rhs;
            case STAR:
                assertNumericOperands(expr.operator, lhs, rhs);
                return (double)lhs * (double)rhs;

            // PLUS is overloaded for string concatenation
            case PLUS:
                if (lhs instanceof Double && rhs instanceof Double) {
                    return (double)lhs + (double)rhs;
                }

                if (lhs instanceof String && rhs instanceof String) {
                    return (String)lhs + (String)rhs;
                } else if (lhs instanceof String) {
                    return lhs + stringify(rhs);
                } else if (rhs instanceof String) {
                    return stringify(lhs) + rhs;
                }

                throw new RuntimeError(expr.operator,
                        "operands must both be numbers or strings.");

            case GREATER:
                assertNumericOperands(expr.operator, lhs, rhs);
                return (double)lhs > (double)rhs;
            case GREATER_EQUAL:
                assertNumericOperands(expr.operator, lhs, rhs);
                return (double)lhs >= (double)rhs;
            case LESS:
                assertNumericOperands(expr.operator, lhs, rhs);
                return (double)lhs < (double)rhs;
            case LESS_EQUAL:
                assertNumericOperands(expr.operator, lhs, rhs);
                return (double)lhs <= (double)rhs;

            case BANG_EQUAL:
                return !isEqual(lhs, rhs);
            case EQUAL:
                return isEqual(lhs,rhs);
        }

        throw new RuntimeError(expr.operator, "Unhandled operator!");
    }

    @Override
    public Object visitCallExpr(Expr.Call expr) {
        Object callee = evaluate(expr.callee);

        List<Object> arguments = new ArrayList<>();
        for (Expr argument : expr.arguments) {
            arguments.add(evaluate(argument));
        }

        // Validate the callee
        if (!(callee instanceof LoxCallable)) {
            throw new RuntimeError(expr.paren,
                    "Can only call functions and classes.");
        }
        LoxCallable function = (LoxCallable)callee;

        // Validate the callee's arity.
        if (function.arity() != arguments.size()) {
            throw new RuntimeError(expr.paren,
                    "Expected " + function.arity() +
                            " arguments, but found " + arguments.size() + ".");
        }

        // Interpret the call.
        return function.call(this, arguments);
    }

    @Override
    public Object visitFunctionExpr(Expr.Function expr) {
        return new LoxFunction("anonymous", expr, environment);
    }

    @Override
    public Object visitGetExpr(Expr.Get expr) {
        Object object = evaluate(expr.object);
        if (object instanceof LoxInstance) {
            return ((LoxInstance)object).get(expr.name);
        }

        throw new RuntimeError(expr.name,
                "Only instances have properties.");
    }

    @Override
    public Object visitTernaryExpr(Expr.Ternary expr) {
        // Evaluate the boolean predicate, then only evaluate the appropriate branch.
        Object test = evaluate(expr.condition);
        return isTruthy(test) ? evaluate(expr.trueClause)
                              : evaluate(expr.falseClause);
    }

    @Override
    public Object visitThisExpr(Expr.This expr) {
        return lookupVariable(expr.keyword, expr);
    }

    private Object evaluate(Expr expression) {
        return expression.accept(this);
    }

    private boolean isTruthy(Object o) {
        // null and false are "false", everything else is "true".
        if (o == null) return false;
        if (o instanceof Boolean) return (boolean)o;
        return false;
    }

    private boolean isEqual(Object lhs, Object rhs) {
        // nil is only equal to nill
        if (lhs == null && rhs == null) return true;
        if (lhs == null) return false;  // null check before de-referencing lhs
        return lhs.equals(rhs);         // defer to Java
    }

    private String stringify(Object o) {
        if (o == null) return "nil";

        // XXX: Remove .0 suffix from integer values.
        if (o instanceof Double) {
            String text = o.toString();
            if (text.endsWith(".0")) {
                text = text.substring(0, text.length() - 2);
            }
            return text;
        }

        return o.toString();
    }

    private void assertNumericOperand(Token operator, Object operand) {
        if (operand instanceof Double) return;
        throw new RuntimeError(operator, "Operand must be numeric.");
    }

    private void assertNumericOperands(Token operator, Object lhs, Object rhs) {
        if (lhs instanceof Double && rhs instanceof Double) return;
        throw new RuntimeError(operator, "Operands must be numeric.");
    }
}
