package net.amtiskaw.lox;

public class RuntimeError extends RuntimeException {
    final Token token;

    RuntimeError(Token tok, String message) {
        super(message);
        token = tok;
    }
}
