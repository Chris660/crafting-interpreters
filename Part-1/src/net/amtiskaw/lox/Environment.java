package net.amtiskaw.lox;

import org.w3c.dom.xpath.XPathResult;

import java.util.HashMap;
import java.util.Map;

public class Environment {
    final Environment enclosing;
    private final Map<String, Object> values = new HashMap<>();

    Environment() {
        enclosing = null;
    }

    Environment(Environment parent) {
        enclosing = parent;
    }

    Object get(Token name) {
        if (values.containsKey(name.lexeme)) {
            return values.get(name.lexeme);
        } else if (enclosing != null) {
            return enclosing.get(name);
        }

        throw new RuntimeError(name,
                "Undefined variable '" + name.lexeme + "'.");
    }

    void define(String name, Object value) {
        values.put(name, value); // NB. allows redefinition of `name`.
    }

    void assign(Token name, Object value) {
        if (values.containsKey(name.lexeme)) {
            values.put(name.lexeme, value);
        } else if (enclosing != null) {
            enclosing.assign(name, value);
        } else {
            throw new RuntimeError(name,
                    "Undefined variable: '" + name.lexeme + "'.");
        }
    }

    public Object getAt(Integer distance, String name) {
        // Return the value for `name` `distance` steps up the environment chain.
        // (where distance == 0 means this Environment).
        return ancestor(distance).values.get(name);
    }

    public void assignAt(Integer distance, Token name, Object value) {
        ancestor(distance).values.put(name.lexeme, value);
    }

    Environment ancestor(Integer distance) {
        Environment result = this;
        for (int i = 0 ; i < distance ; ++i) {
            result = result.enclosing;
        }
        return result;
    }
}
